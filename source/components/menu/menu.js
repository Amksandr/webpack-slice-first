import './menu.scss';

export default function (array, className) {
    var menu = document.createElement('ul');
    menu.className = className;
    var listItem = '';
    array.forEach(element => {
        listItem += '<li>' + element + '</li>';
    });
    menu.innerHTML = listItem;
    return menu;
}