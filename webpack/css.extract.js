// const ExtractTextPlugin = require('extract-text-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = function (paths) {
    return {
        module: {
            rules: [
                {
                    test: /\.(c|sc)ss$/,
                    include: paths,
                    use: [
                        MiniCssExtractPlugin.loader,
                        "css-loader",
                        "sass-loader"
                    ]
                },
                /*
                {
                    test: /\.scss$/,
                    include: paths,
                    use: ExtractTextPlugin.extract({
                        publicPath: '../',
                        fallback: 'style-loader',
                        user: ['css-loader', 'sass-loader']
                    })
                },
                */
                /*
                {
                    test: /\.css$/,
                    include: paths,
                    user: ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        user: 'css-loader'
                    })
                }
                */
            ]
        },
        plugins: [
            // new ExtractTextPlugin('./css/[name].css')
            new MiniCssExtractPlugin({
                // Options similar to the same options in webpackOptions.output
                // both options are optional
                filename: "./css/[name].css",
                // chunkFilename: "[id].css"
            })
        ]
    }
}